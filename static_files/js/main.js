$(document).ready(function () {

    $(window).scroll(function(){
        if ($(window).scrollTop() == $(document).height() - $(window).height()){
            if($('.block').length <= $('#found').val()) {
                getresult('/search?offset=' + $('#found').val());
            }
        }
    });

    $('#loader').hide();

    function getresult(url) {
        $.ajax({
            url: url,
            type: "GET",
            data:  {typesearch:$(".search p").val(),
                    q: $("#text-search").val()},
            beforeSend: function(){
            $('#loader').show();
            },
            complete: function(){
            $('#loader').hide();
            },
            success: function(data){
            $(".blocks ul").append(data);
            },
            error: function(){}
        });
    }

});