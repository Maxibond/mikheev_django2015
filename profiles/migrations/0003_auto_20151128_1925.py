# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_auto_20151128_1925'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profile',
            old_name='owner_cafe',
            new_name='is_owner_cafe',
        ),
    ]
