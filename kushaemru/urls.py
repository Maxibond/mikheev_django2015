"""kushaemru URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from kushaemru import settings
from profiles.views import RegisterFormView

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', 'profiles.views.login', name="login"),
    url(r'^logout/$', 'profiles.views.logout', name="logout"),
    url(r'^register/$', RegisterFormView.as_view()),
    url(r'^settings/$', 'profiles.views.settings', name="settings"),
    url(r'^search/$', 'main.views.search'),
    url(r'^$', 'main.views.index'),
    url(r'^index/$', 'main.views.index', name="index"),
    url(r'^cafes/$', 'main.views.get_cafes'),
    url(r'^cafe/(?P<ids>\d+)/$', 'main.views.get_cafe_id'),
    url(r'^cafe/(?P<ids>\d+)/comments/$', 'main.views.get_cafe_id'),
    url(r'^cafe/(?P<ids>\d+)/comment/$', 'main.views.post_cafe_comment'),
    url(r'^cafe/(?P<ids>\d+)/products/$', 'main.views.get_cafe_products'),
    url(r'^product/(?P<ids>\d+)/$', 'main.views.get_product'),
    url(r'^product/(?P<ids>\d+)/comment/$', 'main.views.post_product_comment'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
