# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from taggit.managers import TaggableManager
# Create your models here.

class Cafe(models.Model):
    name = models.CharField(max_length=100)
    owner = models.ForeignKey(User)
    description = models.TextField(default=u"Нет описания")
    adress = models.TextField(null=True)
    image = models.ImageField(blank=True, upload_to='images/')
    registered_at = models.DateField(null=True)
    last_updated = models.DateField(null=True)
    rating = models.FloatField(default=0)
    
    def calc_rating(self):
        comments = CommentCafe.objects.filter(cafe_id=self.id)
        rating = (0.0+sum([obj.rating for obj in comments]))/len(comments)
        rating = round(rating, 2)
        # x = 0.25 # value for round rating
        # rating = round(rating * (1 / x)) / (1 / x)
        self.rating = rating
        self.save()
        return rating
    
    def __unicode__(self):
        return "%s ( %s... )" % (self.name, self.description[:100])
    
    
class Product(models.Model):
    cafe = models.ForeignKey(Cafe)
    name = models.CharField(max_length=70)
    description = models.TextField(default=u"Нет описания")
    image = models.ImageField(blank=True, upload_to='images/')
    last_updated = models.DateField(null=True)
    tags = TaggableManager()
    rating = models.FloatField(default=0)

    def calc_rating(self):
        comments = CommentProduct.objects.filter(product_id=self.id)
        rating = (0.0+sum([obj.rating for obj in comments]))/len(comments)
        rating = round(rating, 2)
        # x = 0.25 # value for round rating
        # rating = round(rating * (1 / x)) / (1 / x)
        self.rating = rating
        self.save()
        return rating

    
    def __unicode__(self):
        return "%s ( %s... )" % (self.name, self.cafe.name)
    
    
class Comment(models.Model):
    class Meta:
        abstract = True

    user = models.ForeignKey(User)
    rating = models.PositiveSmallIntegerField()
    text = models.CharField(max_length=250)
    created_at = models.DateTimeField()
    
    def __unicode__(self):
        return "%s (%d) : %s" % (self.user.first_name, self.rating,
                                self.text[:50])
    
    
class CommentCafe(Comment):
    cafe = models.ForeignKey(Cafe)


class CommentProduct(Comment):
    product = models.ForeignKey(Product)
    
