from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Cafe)
admin.site.register(Product)
admin.site.register(CommentCafe)
admin.site.register(CommentProduct)
    
