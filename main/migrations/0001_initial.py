# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cafe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('adress', models.TextField()),
                ('image', models.ImageField(upload_to=b'')),
                ('registered_at', models.DateField()),
                ('last_updated', models.DateField()),
                ('rating', models.FloatField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='CommentCafe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.PositiveSmallIntegerField()),
                ('text', models.CharField(max_length=250)),
                ('created_at', models.DateTimeField()),
                ('cafe', models.ForeignKey(to='main.Cafe')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CommentProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.PositiveSmallIntegerField()),
                ('text', models.CharField(max_length=250)),
                ('created_at', models.DateTimeField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=70)),
                ('description', models.TextField()),
                ('image', models.ImageField(upload_to=b'')),
                ('last_updated', models.DateField()),
                ('cafe', models.ForeignKey(to='main.Cafe')),
            ],
        ),
        migrations.AddField(
            model_name='commentproduct',
            name='product',
            field=models.ForeignKey(to='main.Product'),
        ),
        migrations.AddField(
            model_name='commentproduct',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
