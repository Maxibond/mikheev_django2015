from datetime import datetime
from django.shortcuts import render
from django.http import Http404
from django.http import HttpResponseRedirect, HttpResponse as response
from django.contrib.auth.decorators import login_required
from main.models import *
import urllib
from django.http import JsonResponse
from django.db.models import F, Q
import time


def index(request):
    return render(request, "main/search.html", {})


def search(request):
    search_type = request.GET["type-search"]
    search_text = urllib.unquote(request.GET["text-search"])
    if search_type == "cafe":
        cafes = Cafe.objects.raw('SELECT * FROM main_cafe')
        cafes = [cafe for cafe in cafes if search_text.lower() in cafe.name.lower()]
        return render(request, "main/cafes.html", {"cafes": cafes, "searchtext": search_text,
                                                   "found": len(cafes), })
    elif search_type == "product":
        products_tags = Product.objects.filter(tags__name__in=search_text.split(" "))
        products = Product.objects.all()
        result = [product for product in products if search_text.lower() in product.name.lower()] + list(products_tags)
        return render(request, "main/products.html", {"products": result, "searchtext": search_text,
                                                      "found": len(result), })
    return Http404("Search didn't give any result")    


def get_cafes(request):
    cafes = Cafe.objects.order_by("-registered_at")
    return render(request, "main/cafes.html", {"cafes": cafes})


def get_cafe_id(request, ids):
    try:
        cafe = Cafe.objects.get(id=ids)
    except Cafe.DoesNotExist:
        raise Http404("Cafe does not exist")
    coms = CommentCafe.objects.filter(cafe_id=ids).order_by("-created_at")
    return render(request, "main/cafe-comments.html", {"cafe": cafe, "comments": coms,
                                                       "user_login": request.user.is_authenticated()})

@login_required(login_url="/login")
def post_cafe_comment(request, ids):
    comment = CommentCafe(text=request.POST["text-comment"],
                          created_at=datetime.now(),
                          rating=request.POST["rating"],
                          user=request.user,
                          cafe_id=ids,
                          )
    comment.save()
    try:
        Cafe.objects.get(id=ids).calc_rating()
    except Cafe.DoesNotExist:
        raise Http404("Cafe does not exist")     
    return HttpResponseRedirect("/cafe/%s" % ids)


def get_cafe_products(request, ids):
    try:
        cafe = Cafe.objects.get(id=ids)
    except Cafe.DoesNotExist:
        raise Http404("Cafe does not exist")    
    products = Product.objects.filter(cafe_id=ids)
    return render(request, "main/cafe-products.html", {"cafe": cafe, "products": products})


def get_product(request, ids):
    try:
        product = Product.objects.get(id=ids)
    except Product.DoesNotExist:
        raise Http404("Product does not exist")
    q = Q(product_id=ids)
    coms = CommentProduct.objects.filter(q).order_by("-created_at")
    tags = [t for t in product.tags.names()]
    return render(request, "main/product.html", {"product": product, "comments": coms, "tags": tags})

@login_required(login_url="/login")
def post_product_comment(request, ids):
    if "last_comment" in request.session:
        c = time.time() - request.session["last_comment"]
        if c < 10:
            return response("Not so fast, buddy, wait!")
    request.session["last_comment"] = time.time()
    text = request.POST["text-comment"] or "No comment."
    comment = CommentProduct(text=text,
                             created_at=datetime.now(),
                             rating=request.POST["rating"],
                             user=request.user,
                             product_id=ids,
                             )
    comment.save()
    try:
        current = Product.objects.get(id=ids).calc_rating()
    except Product.DoesNotExist:
        raise Http404("Product does not exist")
    return JsonResponse({'user': comment.user.username, 'text': comment.text, 'rating': comment.rating,
                         'created_at': comment.created_at, 'ava': comment.user.profile.avatar.url,
                         'product-rating': current})

